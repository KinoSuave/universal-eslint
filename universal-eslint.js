module.exports = {
	env: {
		browser: true,
		es6: true,
		node: true,
	},
	extends: [
        "plugin:import/recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"prettier",
	],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		project: "tsconfig.json",
		sourceType: "module",
	},
	plugins: [
		"eslint-plugin-jsdoc",
		"eslint-plugin-prefer-arrow",
        "eslint-plugin-import",
		"only-warn",
		"@typescript-eslint",
	],
	rules: {
		"@typescript-eslint/adjacent-overload-signatures": "warn",
		"@typescript-eslint/array-type": [
			"warn",
			{
				default: "array",
			},
		],
		"@typescript-eslint/ban-types": [
			"warn",
			{
				types: {
					Object: {
						message: "Avoid using the `Object` type. Did you mean `object`?",
					},
					Function: {
						message:
							"Avoid using the `Function` type. Prefer a specific function type, like `() => void`.",
					},
					Boolean: {
						message: "Avoid using the `Boolean` type. Did you mean `boolean`?",
					},
					Number: {
						message: "Avoid using the `Number` type. Did you mean `number`?",
					},
					String: {
						message: "Avoid using the `String` type. Did you mean `string`?",
					},
					Symbol: {
						message: "Avoid using the `Symbol` type. Did you mean `symbol`?",
					},
				},
			},
		],
		"@typescript-eslint/consistent-type-assertions": "warn",
		"@typescript-eslint/consistent-type-definitions": "off",
		"@typescript-eslint/dot-notation": "warn",
		"@typescript-eslint/naming-convention": [
            "warn",
            {
                "selector": "interface",
                "format": ["PascalCase"],
                "custom": {
                    "regex": "^I[A-Z]",
                    "match": true
                }
            }
        ],
		"@typescript-eslint/no-empty-function": "off",
		"@typescript-eslint/no-empty-interface": "warn",
		"@typescript-eslint/no-explicit-any": "off",
		"@typescript-eslint/no-misused-new": "warn",
		"@typescript-eslint/no-namespace": "off",
		"@typescript-eslint/no-parameter-properties": "off",
		"@typescript-eslint/no-shadow": [
			"warn",
			{
				hoist: "all",
			},
		],
		"@typescript-eslint/no-misused-promises": "off",
		"@typescript-eslint/no-unsafe-member-access": "off",
		"@typescript-eslint/no-unused-expressions": "warn",
		"@typescript-eslint/no-use-before-define": "off",
		"@typescript-eslint/no-var-requires": "warn",
		"@typescript-eslint/prefer-for-of": "warn",
		"@typescript-eslint/prefer-function-type": "warn",
		"@typescript-eslint/prefer-namespace-keyword": "warn",
		"@typescript-eslint/restrict-template-expressions": "off",
		"@typescript-eslint/triple-slash-reference": [
			"warn",
			{
				path: "always",
				types: "prefer-import",
				lib: "always",
			},
		],
        "@typescript-eslint/strict-boolean-expressions": "warn",
		"@typescript-eslint/unified-signatures": "warn",
		complexity: "off",
		"constructor-super": "warn",
		"dot-notation": "warn",
		"eol-last": "off",
		eqeqeq: ["warn", "smart"],
		"guard-for-in": "warn",
		"id-denylist": [
			"warn",
			"any",
			"Number",
			"number",
			"String",
			"string",
			"Boolean",
			"boolean",
			"Undefined",
			"undefined",
		],
		"id-match": "warn",
        "import/no-unresolved": "error",
        "import/prefer-default-export": "off",
		"import/no-default-export": "warn",
		"import/no-named-as-default": "off",
		"jsdoc/check-alignment": "warn",
		"jsdoc/check-indentation": "warn",
		"jsdoc/newline-after-description": "warn",
		"max-classes-per-file": ["warn", 1],
		"max-len": "off",
		"new-parens": "warn",
		"no-bitwise": "warn",
		"no-caller": "warn",
		"no-cond-assign": "warn",
		"no-console": "off",
		"no-debugger": "warn",
		"no-empty": "off",
		"no-empty-function": "off",
		"no-eval": "warn",
		"no-fallthrough": "off",
		"no-invalid-this": "off",
		"no-new-wrappers": "warn",
		"no-shadow": "off",
		"no-throw-literal": "warn",
		"no-trailing-spaces": "warn",
		"no-undef-init": "warn",
		"no-underscore-dangle": "off",
		"no-unsafe-finally": "warn",
		"no-unused-expressions": "warn",
		"no-unused-labels": "warn",
		"no-use-before-define": "off",
		"no-var": "warn",
		"object-shorthand": "warn",
		"one-var": ["warn", "never"],
		"prefer-arrow/prefer-arrow-functions": "warn",
		"prefer-const": "warn",
		radix: "off",
		"spaced-comment": [
			"warn",
			"always",
			{
				markers: ["/"],
			},
		],
		"use-isnan": "warn",
		"valid-typeof": "off",
	},
    settings: {
        "import/parsers": {
            "@typescript-eslint/parser": [".ts", ".tsx"]
        },
        "import/resolver": {
            "typescript": {
                "alwaysTryTypes": true, // always try to resolve types under `<root>@types` directory even it doesn't contain any source code, like `@types/unist`
            },
        },
    },
};
